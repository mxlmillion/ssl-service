This implements a basic cache system, syncing data to the local FS. When accessed, this service issues a new SSL certificate for the requested domain.

e.g. 
```GET some-domain.com/cert/mydomain.net``` - issues a certificate for mydomain.net and caches the result for future requests.

