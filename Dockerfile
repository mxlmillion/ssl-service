FROM golang:1.10 AS builder
EXPOSE 8010
WORKDIR	/go/src/ssl-service
COPY	. .
RUN	go get -d -v ./...
RUN	go install -v ./...

FROM golang:1.10-alpine
RUN mkdir -p /usr/local/go/{src,pkg,bin}
WORKDIR /usr/local/go/bin
COPY --from=builder /go/src/ssl-service /usr/local/go/src/ssl-service
COPY --from=builder /go/bin /usr/local/go/bin
COPY --from=builder /go/pkg /usr/local/go/pkg
ENTRYPOINT	["ssl-service"]
CMD		["-conf", "/usr/local/go/src/ssl-service/config.json"]
