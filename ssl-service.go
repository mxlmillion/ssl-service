package main

import (
	"fmt"
	"flag"
	"encoding/json"
	"net/http"
	"os"
	"crypto/sha256"
	"encoding/hex"
	
	"github.com/gorilla/mux"
	"github.com/google/uuid"
	"gitlab.com/mxlmillion/ssl-service/log"
	cacher "gitlab.com/mxlmillion/ssl-service/cache"
)

type serviceConfig struct {
	HTTPListenURL string `json:"http_listen_url"`
	HTTPListenPort string `json:"http_listen_port"`
	TLSListenURL string `json:"tls_listen_url"`
	TLSListenPort string `json:"tls_listen_port"`
	TLSCert string `json:"tls_cert_path"`
	TLSKey string `json:"tls_cert_key"`
	CacheSize int `json:"cache_size"`
	CacheDir string `json:"cache_dir"`
	CacheItemTTL int `json:"cache_item_ttl"`
	CacheGCInterval int `json:"cache_gc_interval"`
	LogLevel int `json:"log_level"`
}

var confPath *string
var C *cacher.Cache



func main() {
	// Parse cmd line flags
	confPath = flag.String("conf", `./config.json`, "fs path to configuration file")
	flag.Parse()

	// Get Configuration from file
	conf, _ := getConfiguration()

	// Init log
	log.Init(conf.LogLevel)

	// Init Cache
	cache, err := cacher.Init(conf.CacheDir, conf.CacheSize, conf.CacheItemTTL)
	C = cache
	if err != nil {
		log.Fatal("ERROR: couldn't create cache: %s\n", err)
	}

	// Setup comm channels
	fmt.Printf( "Starting server and listening for errors\n")
	tlserrchan := ServeTLSAPI(conf.TLSListenURL, conf.TLSListenPort, conf.TLSCert, conf.TLSKey)
	errchan := ServeAPI(conf.HTTPListenURL, conf.HTTPListenPort)
	gcchan, selfpollchan := C.StartWorker(conf.CacheGCInterval, conf.TLSCert)

	// Listen for anything worth acting on, or just hang out
	for {
		select {
			case err := <-errchan:
				log.Fatal("ERROR: HTTP server error: %s\nABORTING.\n", err.Error())
			case tlserr := <-tlserrchan:
				log.Fatal("ERROR: TLS server error: %s\nABORTING.\n", tlserr.Error())
			case gcinfo := <-gcchan:
				if gcinfo.StatusId > 0 {
					fmt.Printf( "Cache worker encountered an error: %s\n", gcinfo.Status)
				}
				fmt.Printf( "Cache worker status: %s\n", gcinfo.Status)
			case selfinfo := <-selfpollchan:
				if selfinfo.StatusId > 0 {
					fmt.Printf( "Cache worker encountered an error: %s\n", selfinfo.Status)
				}
				fmt.Printf( "SSL Cert self poll status: %s\n", selfinfo.Status)
		}
	}

	fmt.Printf( "Server quit.\n")
}

func ServeAPI(httplistenurl, httplistenport string ) chan error {
	// Setup handler to redirect to HTTPS
	fmt.Printf("setting up HTTP handler\n")
	r := mux.NewRouter()
	rooth := newHTTPHandle()
	r.Methods("GET").HandlerFunc(rooth.handle)

	// Async listen to potentially do other stuff
	errchan := make(chan error)
	listenurl := httplistenurl + ":" + httplistenport
	go func() { errchan <- http.ListenAndServe(listenurl, r) }()
	return errchan
}

func ServeTLSAPI(tlsurl, tlsport, tlscert, tlskey string) chan error {
	// Setup handler
	fmt.Printf( "setting up TLS handler\n")
	r := mux.NewRouter()
	tlsh := newTLSHandle()
	r.PathPrefix("/cert").HandlerFunc(tlsh.handle)

	// Async listen
	errchan := make(chan error)
	listenurl := tlsurl + ":" + tlsport
	go func() { errchan <- http.ListenAndServeTLS(listenurl, tlscert, tlskey, r) }()
	return errchan
}

func GetSHA256Hash(id string) string {
	o := sha256.New()
	o.Write([]byte(id))
	return hex.EncodeToString(o.Sum(nil))
}

type HTTPReqHandle struct {}
type TLSReqHandle struct {}
func newHTTPHandle() *HTTPReqHandle {
	return &HTTPReqHandle{}
}
func newTLSHandle() *TLSReqHandle {
	return &TLSReqHandle{}
}

func (t *TLSReqHandle) handle(res http.ResponseWriter, req *http.Request) {
	fmt.Printf( "Serving request %s\n", req.URL.Path)

	reqhash := GetSHA256Hash(req.URL.Path)
	if C.Lookup(reqhash) {
		fmt.Printf( "Cert found in cache, serving\n")
		certdata := C.Get(reqhash)
		fmt.Fprintf(res, "cert: %s", certdata)
	} else {
		// Generate cert and store in cache
		// just create a UUID for now
		id := uuid.New()
		certdata := []byte(id.String())
		fmt.Printf( "Generated new cert for \"%s: %s\"\n", req.URL.Path, id)
		C.Put(reqhash, certdata)
		fmt.Fprintf(res, "cert: %s", certdata)
	}

	//time.Sleep(time.Duration(10)*time.Second)
	res.WriteHeader(200)
}

func (t *HTTPReqHandle) handle(res http.ResponseWriter, req *http.Request) {
	// Redirect to HTTPS
	conf, _ := getConfiguration()
	fmt.Printf("Redirecting to HTTPS: %+v\n", req.URL)
	res.Header().Set("Location", "https://" + conf.TLSListenURL + ":" + conf.TLSListenPort + req.URL.Path)
	res.WriteHeader(302)
    //log.Fprintf(res, "config file: = %s", *confPath)
}

func getConfiguration() (serviceConfig, error) {
	fmt.Printf( "Getting config from %s\n", *confPath)
	conf := serviceConfig{}
	f, err := os.Open(*confPath)
	if err != nil {
		fmt.Printf( "WARN: using default values")
		return conf, err
	}
	err = json.NewDecoder(f).Decode(&conf)
	if err != nil {
		log.Fatal("ERROR: parsing json: %s\n", err.Error())
	}
	fmt.Printf( "conf struct: %+v\n", conf)
	return conf, err
}
