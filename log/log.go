package log

import "fmt"
import "time"
import "os"

var level int = 0

func Init(l int) {
	level = l
}

func Log(l int, f string, a ...interface{}) {
	if l > level {
		return
	}
	//do("basic", "<_2>", "< 7>")
	//fmt.Println("type of a: %v\n", a)
	if a == nil {
		fmt.Printf("%s: " + f, time.Now().Local())
	} else {
		fmt.Printf("%s: " + f, time.Now().Local(), a)
	}
}

func Fatal(f string, a ...interface{}) {
	fmt.Printf("%s: " + f, time.Now().Local(), a)
	os.Exit(1)
}

func do(name, layout, want string) {
	t, _ := time.Parse(time.UnixDate, "Sat Mar  7 11:06:39 PST 2015")
	got := t.Format(layout)
	if want != got {
		fmt.Printf("error: for %q got %q; expected %q\n", layout, got, want)
		return
	}
	fmt.Printf("%-15s %q gives %q\n", name, layout, got)
}