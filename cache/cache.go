package cache

import (
	"time"
	"errors"
	"os"
	"fmt"
	"io/ioutil"
	"strconv"
	"sync"
	
	"gitlab.com/mxlmillion/ssl-service/log"
)

type workerstruct struct {
	StatusId int
	Status string
}

type Cache struct {
	maxsize int
	size int
	itemttl int
	evictcount int
	basedir string
	Items map[string][]byte
	Life map[string]int64
	m *sync.Mutex
}

// On program start, init the hash if certs were aready created
func Init(cd string, s int, ttl int) (*Cache, error) {
	if cd[len(cd)-1:] != "/" {
		cd += "/"
	}
	c := newCache(s, ttl, cd)
	certsdir, err := os.Open(cd)
	defer certsdir.Close()
	if err != nil {
		return c, errors.New(err.Error())
	}
	certs, err := certsdir.Readdir(0)
	if err != nil {
		return c, errors.New(err.Error())
	}
	if len(certs) == 0 {
		fmt.Printf("Cache: no pre existing cache to load\n")
		return c, nil
	}

	for _, cert := range certs {
		content, err := ioutil.ReadFile(certsdir.Name() + cert.Name())
		if err != nil {
			log.Fatal("ERROR: opening cert file %s: %s", cert.Name(), err.Error())
		}
		fmt.Printf("Cache adding item %s\n", cert.Name())
		c.Items[cert.Name()] = content
		c.Life[cert.Name()] = cert.ModTime().Unix()
		c.size++
		if c.size >= c.maxsize {
			break
		}
	}

	fmt.Printf("Finished cache init\n")
	return c, nil
}

func newCache(s int, ttl int, cd string) *Cache {
	return &Cache{
		s,
		0,
		ttl,
		0,
		cd,
		make(map[string][]byte),
		make(map[string]int64),
		&sync.Mutex{},
	}
}

func (c *Cache) StartWorker(i int, cert string) (chan workerstruct, chan workerstruct) {
	gcchan := make(chan workerstruct)
	selfpollchan := make(chan workerstruct)
	fmt.Printf("Starting cache gc worker\n")

	// start the GC worker
	go func() {
		for {
			gcchan <- c.clean()
			time.Sleep(time.Duration(i)*time.Second)
		}
	}()

	// start polling ssl cert of this server, renew when expired
	go func() {
		for {
			selfpollchan <- c.pollselfcert(cert)
			time.Sleep(time.Duration(3600)*time.Second)
		}
	}()
	return gcchan, selfpollchan
}

func (c *Cache) pollselfcert(cert string) workerstruct {
	// implemented this later, check local ssl cert and renew if needed
	return workerstruct{
		0,
		cert,
	}
}

func (c *Cache) clean() workerstruct {
	certsdir, err := os.Open(c.basedir)
	defer certsdir.Close()
	if err != nil {
		return workerstruct{
			1,
			err.Error(),
		}
	}
	certs, err := certsdir.Readdir(0)
	if err != nil {
		return workerstruct{
			2,
			err.Error(),
		}
	}

	for _, cert := range certs {
		// Decide if we should evict item
		d := int(time.Now().Unix()) - int(cert.ModTime().Unix())
		//fmt.Printf("file \"%s\" change delta: %d\n", cert.Name(), d)
		if d >= c.itemttl {
			err := os.Remove(c.basedir + cert.Name())
			if err != nil {
				return workerstruct{
					3,
					"Unable to clear expired item!: " + err.Error(),
				}
			}
			c.Delete(cert.Name())
		}
	}

	return workerstruct{
		0,
		c.GetStats(),
	}
}

func (c *Cache) Size() int {
	c.m.Lock()
	size := c.size
	c.m.Unlock()
	return size
}

func (c *Cache) Delete(h string) {
	c.m.Lock()
	c.evictcount++
	c.size--
	delete(c.Items, h)
	delete(c.Life, h)
	c.m.Unlock()
}

func (c *Cache) Get(h string) []byte {
	c.m.Lock()
	item := c.Items[h]
	c.m.Unlock()
	return item
}

func (c *Cache) GetStats() string {
	c.m.Lock()
	max := strconv.Itoa(c.maxsize)
	size := strconv.Itoa(c.size)
	ecount := strconv.Itoa(c.evictcount)
	c.m.Unlock()
	return "max size: " + max + ", current size: " + size + ", evict count: " + ecount
}

func (c *Cache) Lookup(h string) bool{
	c.m.Lock()
	_, ok := c.Items[h]
	c.m.Unlock()
	if ! ok {
		fmt.Printf("ID NOT FOUND\n")
	} else {
		fmt.Printf("Cache hit for %s\n", h)
	}
	return ok
}

func (c *Cache) Put(h string, id []byte) {
	c.m.Lock()
	bdir := c.basedir
	size := c.size
	msize := c.maxsize
	if size >= msize {
		c.m.Unlock()
		fmt.Printf("Cache maximum size reached")
		return
	}
	c.m.Unlock()

	// Write to filesystem for resiliency
	err := ioutil.WriteFile(bdir + h, id, 0666)
	if err != nil {
		fmt.Printf("WARN: couldn't write new cert to cache: %s", err)
		return
	}

	// Add to current Cache
	c.m.Lock()
	c.Items[h] = id
	c.Life[h] = time.Now().Unix()
	c.size++
	c.m.Unlock()
}
